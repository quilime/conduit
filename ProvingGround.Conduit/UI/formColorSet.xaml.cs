﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Xceed.Wpf.Toolkit;

namespace ProvingGround.Conduit.UI
{
    /// <summary>
    /// Interaction logic for ucColor.xaml
    /// </summary>
    public partial class formColorSet : Window
    {

        public System.Windows.Media.Color _colorSet;

        public formColorSet(System.Windows.Media.Color ColorSet)
        {
            InitializeComponent();

            ColorCanvas_set.R = ColorSet.R;
            ColorCanvas_set.G = ColorSet.G;
            ColorCanvas_set.B = ColorSet.B;

            _colorSet = ColorSet;
        }

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            _colorSet = System.Windows.Media.Color.FromRgb(ColorCanvas_set.R, ColorCanvas_set.G, ColorCanvas_set.B);
            this.Close();
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
