﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.Kernel;

using Rhino;
using Rhino.Collections;
using Rhino.Geometry;

using System.Windows.Media;
using System.Windows.Media.Imaging;

using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace ProvingGround.Conduit.Classes
{
    /// <summary>
    /// Axis Settings
    /// </summary>
    [System.Xml.Serialization.XmlRootAttribute("ChartStyle")]
    public class clsChartStyle : acStyle
    {
        [System.Xml.Serialization.XmlElementAttribute("StyleName")]
        public override string styleName { get; set; }

        [XmlIgnore]
        public override bool unset { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("AxisPadding")]
        public double axisPadding { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CategoryPadding")]
        public double categoryPadding { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CategorySpacing")]
        public double categorySpacing { get; set; }

        public clsChartStyle()
        { }

        public clsChartStyle(double AxisPadding, double CategoryPadding, double CategorySpacing)
        {
            axisPadding = AxisPadding;
            categoryPadding = CategoryPadding;
            categoryPadding = CategorySpacing;
        }

        public static List<clsChartStyle> defaults()
        {
            List<clsChartStyle> setDefaults = new List<clsChartStyle>();

            return setDefaults;
        }

    }
}

